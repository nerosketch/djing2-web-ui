const state = {
  err_dialog_state: false,
  err_dialog_text: '',
  err_dialog_title: '',
  err_snackbar_text: '',
  err_snackbar_show: false
};


const getters = {
  DIALOG_STATE(state) {
    return state.err_dialog_state
  },
  DIALOG_TEXT(state) {
    return state.err_dialog_text
  },
  DIALOG_TITLE(state) {
    return state.err_dialog_title
  },
  SNACK_STATE(state) {
    return state.err_snackbar_show
  },
  SNACK_TEXT(state) {
    return state.err_snackbar_text
  }
};


const mutations = {
  SHOW_DIALOG(state, data) {
    state.err_dialog_text = data.text
    state.err_dialog_title = data.title
    state.err_dialog_state = true
  },
  CLOSE_DIALOG(state) {
    state.err_dialog_state = false
  },
  SNACK_SHOW(state, text) {
    // console.log('snack show', state.err_snackbar_show);
    state.err_snackbar_text = text
    state.err_snackbar_show = true
  },
  SNACK_CLOSE(state) {
    state.err_snackbar_show = false
  },
  SNACK_SET(s) {
    state.err_snackbar_show = s;
  }
};


export default {
  state,
  getters,
  mutations,
  // actions
};