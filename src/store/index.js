import Vue from 'vue'
import Vuex from 'vuex'
import errors from './errors'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {},
    getters: {},
    mutations: {},
    actions: {},
    modules: {
        errors
    }
})
