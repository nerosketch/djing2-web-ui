export default {
  methods: {
    showDialog(text, title = "Error") {
      this.$store.commit("SHOW_DIALOG", { text, title });
    },
    showMessage(text) {
      this.$store.commit("SNACK_SHOW", text);
    },
  }
}
