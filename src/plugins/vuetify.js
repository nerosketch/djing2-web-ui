import Vue from 'vue'
import Vuetify from 'vuetify/lib'
// import 'vuetify/src/stylus/app.styl'
import '@mdi/font/css/materialdesignicons.css'

// import { Ripple } from 'vuetify/lib/directives'

Vue.use(Vuetify)

export default new Vuetify()
