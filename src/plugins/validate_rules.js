'use strict'

const t = 'invalid'

const ruleWithParameter = (fn, param) => val => (fn(val, param))

/*
 * decorator for allow empty values, For prevent it use required
 */
const Emv = fn => v => (v ? fn(v) : true)

export default {
    telephone: Emv(v => (/^(\+[7893]\d{10,11})?$/i.test(v)) || `${t} telephone`),

    ip_addr: Emv(v => (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/i.test(v)) || `${t} ip addr`),

    mac_addr: Emv(v => (/^([0-9A-Fa-f]{1,2}[:-]){5}([0-9A-Fa-f]{1,2})$/i.test(v)) || `${t} mac address`),

    required: v => (v ? String(v).length > 0 : false) || 'Обязательное поле',

    email: Emv(v => (/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/i.test(v)) || `${t} email`),

    uname: Emv(v => (/^\w{1,127}$/i.test(v)) || t),

    maxLen: Emv(l => ruleWithParameter((v, p) => (String(v).length <= p) || 'Слишком длинное значение', l)),

    isDigit: Emv(v => (!isNaN(Number(v))) || 'Number required'),

    digitPositive: Emv(v => (Number(v) > 0) || 'Требуется положительное число')
}
