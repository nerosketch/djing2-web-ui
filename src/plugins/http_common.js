import axios from 'axios'
// import qs from 'qs'
import Vue from 'vue'
import { store } from '../store/index'
import router from '../router'


export const HTTP = axios.create({
	baseURL: 'http://127.0.0.1:8000/api',
  /*transformRequest: [dat => {
    / *if (dat)
      for (const [k, v] of Object.entries(dat)) {
        const t = typeof v;
        if (t == null || t == undefined) {
          delete dat[k];
        }
      }* /
    return qs.stringify(dat)
  }],*/
  withCredentials: true,
  // headers: {'Content-Type': 'application/json'}
})


export const HTTP_MOD = {
  install() {
    Vue.prototype.$http = HTTP;
  },
  HTTP
}


HTTP.interceptors.request.use(
  config => {
    if (!config.headers.Authorization) {
      const token = localStorage.authToken;

      if (token) {
        config.headers.Authorization = `Token ${token}`;
      }
    }

    return config;
  },
  error => Promise.reject(error)
);


HTTP.interceptors.response.use(undefined, e => {
  // const originalRequest = e.config;
  if (!e.response) {
    store.commit('SHOW_DIALOG', { text: 'Нет связи', title: 'Сеть' });
  } else if (e.response.status == 403) {
    store.commit('SHOW_DIALOG', { text: e.response.data.detail, title: e.response.statusText });
    setTimeout(() => {
      router.push("/login/");
    }, 3000);
  } else {
    store.commit('SHOW_DIALOG', { text: e.response.data, title: e.response.statusText });
  }
  // console.log('HTTP.interceptors.response', e.response, originalRequest);

  /*if (e.response.status === 401 && !originalRequest._retry) {
    return new Promise((resolve, reject) => {

    })
  }*/
  return Promise.reject(e);
});
