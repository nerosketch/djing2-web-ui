import VAL from "../validate_rules";

export default {
  props: {
    value: undefined,
    autofocus: {
      type: Boolean,
      default: false
    },
    required: {
      type: Boolean,
      default: false
    },
    ic: String
  },
  data() {
    return {
      errs: "",
      rul: this.required ? [VAL.required] : []
    }
  }
}