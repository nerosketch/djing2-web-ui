import Vue from 'vue'
import Router from 'vue-router'

import routerPlace from './plugins/routerPlace'

const AccountsList = () => import('./components/accounts/accounts-list')
const Account = () => import('./components/accounts/Account')
const AccountForm = () => import('./components/accounts/AccountForm')
const GroupsList = () => import('./components/groups/groups-list')
const Login = () => import('./login')
import Billing from './Billing'
const serviceList = () => import('./components/services/servicesList')
const gwList = () => import('./components/gateways/gwList')
const DeviceGroupList = () => import('./components/devices/groupList')
const DeviceList = () => import('./components/devices/list')
const deviceView = () => import('./components/devices/deviceView')
const CustomersList = () => import('./components/customers/customers-list')
const CustomersGroupList = () => import('./components/customers/groupList')
const CustomerDetails = () => import('./components/customers/details')

const CustomerFinance = () => import('./components/customers/details/finance')
const CustomerTaskHistory = () => import('./components/customers/details/history')
const CustomerInfo = () => import('./components/customers/details/info')
const CustomerServices = () => import('./components/customers/details/services')
const grpServices = () => import('./components/customers/grpServices')

const tasksList = () => import('./components/tasks/tasksList')
const taskDetails = () => import('./components/tasks/taskDetails')

Vue.use(Router)

const groupDevProp = r => ({ groupId: Number(r.params.groupId), devId: Number(r.params.devId) })
const uidProp = r => ({ groupId: Number(r.params.groupId), uid: Number(r.params.uid) })
const gidProp = r => ({ groupId: Number(r.params.groupId) })
const taskProp = r => ({ taskId: Number(r.params.taskId) })

function breadcrumbName(rp) {
    let e;
    for (const o of Object.entries(rp)) {
        e = o[1];
    }
    return e
}

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        { path: '/', redirect: '/customers/' },
        {
            path: '/login',
            component: Login,
            children: []
        },
        {
            path: '*',
            component: Billing,
            children: [
                {
                    path: '/accounts/',
                    component: routerPlace,
                    meta: { breadcrumb: 'Учётные записи' },
                    children: [
                        {
                            path: '',
                            name: 'accounts',
                            component: AccountsList
                        },
                        {
                            path: 'add/',
                            name: 'accounts_add',
                            component: AccountForm,
                            meta: {
                                breadcrumb: 'Добавить учётную запись'
                            }
                        },
                        {
                            path: ':puname/',
                            component: routerPlace,
                            meta: {
                                breadcrumb: breadcrumbName
                            },
                            children: [
                                {
                                    path: '',
                                    component: Account,
                                    name: 'account_link',
                                    props: true
                                },
                                {
                                    path: 'edit/',
                                    component: AccountForm,
                                    name: 'account_edit_link',
                                    props: true,
                                    meta: { breadcrumb: 'Редактировать' },
                                },
                            ]
                        },
                    ]
                },
                {
                    path: '/groups/',
                    component: GroupsList,
                    name: 'groups_link'
                },
                {
                    path: '/services/',
                    component: serviceList,
                    name: 'serviceList'
                },
                {
                    path: '/gateways/',
                    component: gwList,
                    name: 'gatewayList'
                },
                {
                    path: '/devices/',
                    component: routerPlace,
                    meta: { breadcrumb: 'Группы устрофств' },
                    children: [
                        {
                            path: '',
                            component: DeviceGroupList,
                            name: 'devGroupList',
                        },
                        {
                            path: ':groupId/',
                            component: routerPlace,
                            meta: { breadcrumb: 'Устройства' },
                            children: [
                                {
                                    path: '',
                                    component: DeviceList,
                                    name: 'devList',
                                    props: gidProp
                                },
                                {
                                    path: ':devId/',
                                    component: deviceView,
                                    name: 'deviceView',
                                    props: groupDevProp,
                                    meta: { breadcrumb: 'Устр имя' },
                                }
                            ]
                        },
                    ]
                },
                {
                    path: '/customers/',
                    component: routerPlace,
                    meta: { breadcrumb: 'Группы абонентов' },
                    children: [
                        {
                            path: '',
                            component: CustomersGroupList,
                            name: 'customersGroupList',
                        },
                        {
                            path: ':groupId/',
                            component: routerPlace,
                            meta: { breadcrumb: 'Группа ххх' },
                            children: [
                                {
                                    path: '',
                                    component: CustomersList,
                                    name: 'customersList',
                                    props: gidProp,
                                },
                                {
                                    path: 'attach-group-service/',
                                    component: grpServices,
                                    name: 'grpServices',
                                    props: gidProp
                                },
                                {
                                    path: ':uid/',
                                    component: CustomerDetails,
                                    meta: { breadcrumb: '[абона имя]' },
                                    children: [
                                        {
                                            path: '',
                                            component: CustomerInfo,
                                            name: 'customerInfo',
                                            props: uidProp,
                                        },
                                        {
                                            path: 'services/',
                                            component: CustomerServices,
                                            name: 'customerServices',
                                            props: uidProp,
                                        },
                                        {
                                            path: 'finance/',
                                            component: CustomerFinance,
                                            name: 'customerFinance',
                                            props: uidProp,
                                        },
                                        {
                                            path: 'task-history/',
                                            component: CustomerTaskHistory,
                                            name: 'customerTaskHistory',
                                            props: uidProp,
                                        }
                                    ]
                                },
                            ]
                        }
                    ]
                },
                {
                    path: '/tasks/',
                    component: routerPlace,
                    meta: { breadcrumb: 'Задачи' },
                    children: [
                        {
                            path: '',
                            component: tasksList,
                            name: 'tasksList',
                        },
                        {
                            path: ':taskId/',
                            component: taskDetails,
                            name: 'taskDetails',
                            props: taskProp,
                            meta: { breadcrumb: '[заявка]' },
                        }
                    ]
                }
            ]
        }
    ]
})
