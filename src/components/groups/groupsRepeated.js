export default {
  methods: {
    fetchGrps() {
      this.$http.get("/groups/").then(r => {
        this.groups = r.data;
      });
    }
  },
  computed: {
    groupsExists() {
      return this.groups.length > 0;
    }
  },
  data() {
    return {
      groups: []
    }
  }
}