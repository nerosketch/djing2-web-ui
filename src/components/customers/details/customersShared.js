export default {
  props: {
    uid: Number,
    groupId: {
      type: Number,
      default: 0
    }
  },
  mounted() {
    this.fetchUser();
  },
  methods: {
    fetchUser() {
      this.$http.get(`/customers/${this.uid}/`).then(r => {
        this.customer = Object.assign({}, r.data);
      });
    }
  },
  data() {
    return {
      customer: {}
    };
  }
}