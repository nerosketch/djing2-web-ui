import Vue from 'vue'
import vuetify from './plugins/vuetify'
import App from './App.vue'
import router from './router'

import 'material-design-icons-iconfont/dist/material-design-icons.css'
import { store } from './store'
import { HTTP_MOD } from './plugins/http_common'
import vuetifyBreadcrumbs from "./plugins/breadcrumbs"

Vue.config.productionTip = false

Vue.use(HTTP_MOD)
Vue.use(vuetifyBreadcrumbs)

new Vue({
  store,
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
